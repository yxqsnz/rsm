#[derive(Clone)]
pub struct Task {
    pub runs: Vec<String>,
}
use std::collections::HashMap;
#[derive(Clone)]
pub struct RustMakeBuildFile {
    pub verbose: String,
    pub constants: HashMap<String, String>,
    pub tasks: HashMap<String, Task>,
}
pub enum BuildFileTypes {
    TOML,
    YAML,
}
