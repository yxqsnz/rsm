use crate::{logger::Logger, structs::rsm::RustMakeBuildFile};
use std::process::{exit, Command};
use std::str::from_utf8;
fn get_constant_value(raw: &String) -> String {
    fn run_code_and_get_result(code: &String) -> String {
        from_utf8(
            Command::new("bash")
                .args(&["-c", code, ">> /dev/null"])
                .output()
                .unwrap()
                .stdout
                .as_slice(),
        )
        .unwrap()
        .to_string()
    }
    let mut result: String;
    if raw.starts_with("exec:") {
        result = raw.replace("exec:", "").trim().to_string();
        result = run_code_and_get_result(&result);
    } else {
        result = raw.clone();
    }
    result
}
fn run_command(com: &str) -> bool {
    let r = Command::new("bash").args(&["-c", com]).spawn();
    match r {
        Ok(mut child) => match child.wait() {
            Ok(s) => s.success(),
            Err(_) => false,
        },
        Err(_) => false,
    }
}
fn run_command_quiet(com: &str) -> bool {
    let out = Command::new("bash").args(&["-c", com]).output();
    out.is_ok()
}
pub fn run_rsm_file(rsm: RustMakeBuildFile, sl_task: String, exit_on_command_fail: bool) {
    let mut log = Logger::new("RsmBuildFile");
    if !rsm.tasks.contains_key(&sl_task) {
        log.error(format!("Task '{}' is invalid!", sl_task).as_str());
        exit(1);
    }
    let task = &rsm.tasks[&sl_task];
    for run in &task.runs {
        let mut runb = run.clone();
        if runb.contains("$(") {
            for (key, value) in &rsm.constants {
                runb = runb.replace(
                    format!("$({})", key).as_str(),
                    get_constant_value(&value.to_string()).as_str(),
                )
            }
        }
        if runb.contains("exec:") {
            runb = runb.replace("exec:", "").trim().to_string();
            if rsm.verbose == *"nil" {
                if !run_command_quiet(&runb) && exit_on_command_fail {
                    if rsm.verbose != *"nil" {
                        log.error(format!("Command '{}' Failed", runb).as_str());
                    }
                    std::process::exit(1);
                }
            } else if !run_command(&runb) && exit_on_command_fail {
                if rsm.verbose != *"nil" {
                    log.error(format!("Command '{}' failed.", runb).as_str())
                }
                std::process::exit(1)
            }
        }
        if runb.contains("task:") {
            let mut tsk: String = String::from("clean");
            let spl: Vec<&str> = runb.split(":").collect();
            if spl.len() > 1 {
                tsk = spl[1].trim().to_string();
            }
            run_rsm_file(rsm.clone(), tsk, exit_on_command_fail)
        }
    }
}
