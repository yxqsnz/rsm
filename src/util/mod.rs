pub mod rsm;
pub mod rsm_file;
use crate::{logger::Logger, types::*};
use std::path::Path;

pub fn rsm_file_exists() -> Boolean {
    Path::new("./rsm.build").exists()
}
pub fn setup_panic() {
    std::panic::set_hook(Box::new(|panic| {
        let mut log = Logger::new("PANIC!");
        log.error(format!("Sorry!!: {:#?}", panic).as_str());
    }));
}
