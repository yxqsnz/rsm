use crate::structs::rsm::*;

use crate::{logger::Logger, structs::rsm::Task};
use std::{collections::HashMap, fs, vec};
use yaml_rust::YamlLoader;
#[derive(Debug)]
pub struct RustMakeBuildFileError;
pub fn get_file_type(file: &str) -> BuildFileTypes {
    let file_content = fs::read_to_string(file).unwrap_or(String::from("#? YAML"));
    let file_split: Vec<&str> = file_content.split("\n").collect();
    let first_line = file_split[0];
    let content_s: Vec<&str> = first_line.trim().split("#?").collect();
    if content_s.len() < 1 {
        return BuildFileTypes::YAML;
    }
    let tp = content_s[1].trim().to_uppercase();
    if tp == *"YAML" {
        return BuildFileTypes::YAML;
    } else {
        return BuildFileTypes::TOML;
    }
}
pub fn get_file_content(path: &str) -> Option<String> {
    let mut log = Logger::new("RustMake/Reader");
    let file = fs::read_to_string(path);
    match file {
        Ok(content) => {
            let mut split: Vec<&str> = content.split("\n").collect();
            if split.len() > 1 {
                if split[0].starts_with("#?") {
                    split.remove(0);
                }
                return Some(split.join("\n").to_string());
            }
        }
        Err(e) => {
            log.error(format!("Failed to read '{}': {:#?} ", path, e).as_str());
            std::process::exit(1);
        }
    }
    None
}
pub fn read_toml(file_content: &str) -> Result<RustMakeBuildFile, RustMakeBuildFileError> {
    let mut result = RustMakeBuildFile {
        tasks: HashMap::new(),
        verbose: "normal".to_string(),
        constants: HashMap::new(),
    };
    let tml: toml::Value = toml::from_str(file_content).unwrap();
    if let Some(consts_v) = tml.get("constants") {
        if let Some(consts) = consts_v.as_table() {
            for (name, value) in consts {
                if let Some(v) = value.clone().as_str() {
                    result
                        .constants
                        .insert(name.clone().as_str().to_string(), v.to_string());
                }
            }
        }
    }
    if let Some(tasks_v) = tml.get("tasks") {
        if let Some(tasks) = tasks_v.clone().as_table() {
            for (name, value) in tasks {
                let mut task = Task { runs: vec![] };
                if let Some(runs) = value["run"].clone().as_array() {
                    for run_v in runs {
                        if let Some(run) = run_v.as_str() {
                            task.runs.push(run.to_string());
                        }
                    }
                }
                result.tasks.insert(name.to_string(), task);
            }
        }
    }
    Ok(result)
}
pub fn read_yaml(file_content: &str) -> Result<RustMakeBuildFile, RustMakeBuildFileError> {
    let mut log = Logger::new("RustMakeBuildFileParse");

    let result = YamlLoader::load_from_str(file_content);
    match result {
        Ok(yaml) => {
            if yaml.len() == 0 {
                return Err(RustMakeBuildFileError);
            }
            let file = &yaml[0];
            let verbose: String = if let Some(v) = file["verbose"].as_str() {
                String::from(v)
            } else {
                String::from("normal")
            };
            let mut g_tasks: HashMap<String, Task> = HashMap::new();

            let mut constants = HashMap::new();

            if let Some(tasks) = file["tasks"].clone().into_vec() {
                for task in tasks {
                    let task_hash = task.into_hash();
                    if let Some(tsk) = task_hash {
                        for (key, value) in tsk {
                            if let Some(task_name) = key.into_string() {
                                if let Some(task_content) = value.into_vec() {
                                    let mut x = Task { runs: vec![] };
                                    for it in task_content {
                                        if let Some(st) = it.clone().into_string() {
                                            x.runs.push(st);
                                        }
                                    }

                                    g_tasks.insert(task_name, x);
                                }
                            }
                        }
                    }
                }
            }
            if let Some(consts) = file["constants"].clone().into_vec() {
                for cons in consts {
                    if let Some(hs) = cons.into_hash() {
                        for (key, value) in hs {
                            if let Some(k) = key.into_string() {
                                if let Some(v) = value.into_string() {
                                    constants.insert(k, v);
                                }
                            }
                        }
                    }
                }
            }

            Ok(RustMakeBuildFile {
                constants,
                tasks: g_tasks,
                verbose,
            })
        }
        Err(error) => {
            log.error(format!("Failed to parse YAML!: {:#?}", error).as_str());
            Err(RustMakeBuildFileError)
        }
    }
}
