//#![deny(unused_variables)]
use logger::Logger;
use rsm::structs::rsm::BuildFileTypes;
use rsm::{logger, util};
use std::env;
use std::process::exit;
use util::rsm as rs;

fn main() {
    let mut log = Logger::new("RSM/Main");
    util::setup_panic();
    let args: Vec<String> = env::args().collect();
    let task: String;
    let build_file = "rsm.build";
    let mut ignore_errors = true;
    if args.len() > 1 {
        if args[1] == "-h" || args[1] == "--help" {
            println!("--help: Show this message\n-ie or --ignore-errors: ignore the errors.");
            std::process::exit(1);
        }
        task = args[1].clone();
    } else {
        task = String::from("main");
    }
    if !util::rsm_file_exists() {
        log.error("Please create a rsm file!: `rsm.build`");
        exit(-1);
    }
    if args.len() > 2 {
        if args[2] == "-ie" || args[2] == "--ignore-errors" {
            ignore_errors = false
        }
    }
    let file_type = util::rsm_file::get_file_type(build_file);
    let build_file_content = util::rsm_file::get_file_content(build_file).unwrap();

    match file_type {
        BuildFileTypes::TOML => {
            let toml = util::rsm_file::read_toml(&build_file_content);
            match toml {
                Ok(rsm) => {
                    if rsm.tasks.contains_key(&task) {
                        rs::run_rsm_file(rsm, task, ignore_errors);
                    } else {
                        log.error(format!("Task: '{}' does not exist", task,).as_str())
                    }
                }
                Err(_) => {
                    println!("...");
                    std::process::exit(1);
                }
            }
        }
        BuildFileTypes::YAML => {
            let rsm_file = util::rsm_file::read_yaml(&build_file_content);
            match rsm_file {
                Ok(rsm) => {
                    if rsm.tasks.contains_key(&task) {
                        rs::run_rsm_file(rsm, task, ignore_errors);
                    } else {
                        log.error(format!("Task: '{}' does not exist", task,).as_str())
                    }
                }
                Err(_) => {
                    println!("...");
                    std::process::exit(1);
                }
            }
        }
    }

    // let rsm_file = util::rsm_file::read();
    // match rsm_file {
    //     Ok(rsm_file) => {
    //         if rsm_file.tasks.contains_key(&task) {
    //             xb::run_rsm_file(rsm_file, task, ignore_errors);
    //         } else {
    //             log.error(format!("Task: '{}' does not exist.", task).as_str())
    //         }
    //     }
    //     Err(_) => {
    //         std::process::exit(1);
    //     }
    // }
}
