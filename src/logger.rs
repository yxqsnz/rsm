use chrono;
use colored::*;
#[derive(Clone)]
pub struct Logger {
    scope: String,
}
impl Logger {
    pub fn new(scope: &str) -> Self {
        Self {
            scope: scope.to_string(),
        }
    }

    fn get_time(&self) -> String {
        let dt = chrono::Local::now();
        dt.format("%H:%M:%S").to_string()
    }
    pub fn info(&mut self, content: &str) {
        let x = format!(
            "{} on {} at {}: {}",
            "information".truecolor(98, 114, 164),
            self.scope.truecolor(255, 121, 198),
            self.get_time(),
            content.truecolor(98, 114, 164)
        );

        println!("{}", x)
    }
    pub fn error(&mut self, content: &str) {
        let x = format!(
            "{} on {} at {}: {}",
            "error".truecolor(255, 85, 85),
            self.scope.truecolor(255, 121, 198),
            self.get_time(),
            content.truecolor(98, 114, 164)
        );

        println!("{}", x)
    }
    pub fn warn(&mut self, content: &str) {
        let x = format!(
            "{} on {} at {}: {}",
            "warning".truecolor(241, 250, 140),
            self.scope.truecolor(255, 121, 198),
            self.get_time(),
            content.truecolor(98, 114, 164)
        );

        println!("{}", x);
    }
}
