# Rust Make

![total_lines](https://tokei.rs/b1/gitlab/yxqsnz/rsm)

## Rust Make is a simple build system written in Rust *NOT COMPATIBLE WITH WINDOWS YET.*

## Example Rust Make Files `rsm.build`

### YAML

```yml
#? YAML
verbose: 'normal'

constants:
  - text: Hello, world!
  - text-br: Olá, Mundo!
tasks:
  - all:
    - "exec: echo $(text)"
    - "exec: echo $(text-br)"
  - test: 
    - "exec: echo $(text)"
```

## TOML

```toml
#? TOML

[constants]
text = "Hello, world!"
text-br = "Olá, Mundo!"
[tasks] 
  [tasks.main]
  run = [
    "exec: echo $(text-br)",
    "exec: echo $(text)",
    "task: test"
  ]
  [tasks.test]
  run = [
    'exec: echo $(text)'
  ]
```

## Running

## Requirements

- [Rust](http://rustup.rs/)

### Run `cargo run` or `cargo r --release`

### Install `cargo install rsm`
